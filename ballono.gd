extends Sprite2D

var lifetime : float = 0
var original_position : Vector2 = Vector2.ZERO
@export_range(-10,10,0.1) var drift : float = 0.0
@export_range(0,20,0.1)  var sway_amount : float = 0.0
@export_range(0.0,4,0.2) var sway_frequency : float = 0.0
@export_range(0.2,10.0,0.1) var balloon_size : float = 8
@export_range(60,250,0.1) var speed : float = 100

@onready var audio = $AudioStreamPlayer2D

var explosion1 = preload("res://assets/sounds/pop.wav")
var explosion2 = preload("res://assets/sounds/pop2.wav")
var explosion3 = preload("res://assets/sounds/pop3.wav")

var defined_color = 0

var red_start_texture = preload("res://assets/balloons/red-balloon.png")
var red_highlighted_texture = preload("res://assets/balloons/red-balloon-highlighted.png")

var blue_start_texture = preload("res://assets/balloons/blue-balloon.png")
var blue_highlighted_texture = preload("res://assets/balloons/blue-balloon-highlighted.png")

var green_start_texture = preload("res://assets/balloons/green-balloon.png")
var green_highlighted_texture = preload("res://assets/balloons/green-balloon-highlighted.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	defined_color = randi_range(1,3)
	if defined_color == 1:
		texture = blue_start_texture
	if defined_color == 2:
		texture = red_start_texture
	if defined_color == 3:
		texture = green_start_texture
	position.y = 755
	original_position = Vector2(randf_range(56,1098),755)
	scale = Vector2(balloon_size,balloon_size)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !Input.is_action_just_pressed("Click"):
		G.clicked_balloon = false
	lifetime += delta
	position.y -= speed * delta
	position.x = original_position.x + sway_amount * sin(lifetime * sway_frequency) + drift * lifetime
	if position.y <= -156:
		randomize()
		position.y = 755
		original_position = Vector2(randf_range(100,1060),755)
	pass
func _on_balloon_hit_box_mouse_entered():
	if defined_color == 1:
		texture = blue_highlighted_texture
	if defined_color == 2:
		texture = red_highlighted_texture
	if defined_color == 3:
		texture = green_highlighted_texture



func _on_balloon_hit_box_mouse_exited():
	if defined_color == 1:
		texture = blue_start_texture
	if defined_color == 2:
		texture = red_start_texture
	if defined_color == 3:
		texture = green_start_texture


func _on_balloon_hit_box_input_event(viewport : Viewport, event, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_LEFT: #and Input.is_action_just_pressed()
		if G.clicked_balloon == false:
			G.clicked_balloon = true
			G.score += 1
			if get_rect().has_point(to_local(event.position)):
				viewport.set_input_as_handled()
				self.hide()
				if randi_range(1,3) == 1:
					audio.stream = explosion1
				elif randi_range(1,2) == 1:
					audio.stream = explosion2
				else:
					audio.stream = explosion3
				audio.play()
				await  get_tree().create_timer(0.3).timeout
				defined_color = randi_range(1,3)
				if defined_color == 1:
					texture = blue_start_texture
				if defined_color == 2:
					texture = red_start_texture
				if defined_color == 3:
					texture = green_start_texture
				show()
				randomize()
				position.y = 755
				original_position = Vector2(randf_range(100,1030) - 75,755)
