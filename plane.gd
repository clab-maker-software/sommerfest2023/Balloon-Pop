extends Sprite2D

@export_range(70,200,1) var speed = 100

@onready var audio = $AudioStreamPlayer2D

var normal_plane = preload("res://assets/plane.png")
var selected_plane = preload("res://assets/plane_highlighted.png")

func _ready():
	randomize()
	position.x = 1261
	position.y = randi_range(400,90)

func _process(delta):
	position.x -= speed * delta 
	if position.x <= -123:
		position.x = 1333
		position.y = randi_range(400,90)


func _on_plane_hit_box_mouse_entered():
	texture = selected_plane


func _on_plane_hit_box_mouse_exited():
	texture = normal_plane


func _on_plane_hit_box_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_LEFT:
		hide()
		G.score -= 3
		audio.play()
		await get_tree().create_timer(randf_range(1.5,2.5))
		position.x = 1333
		position.y = randi_range(400,90)
		show()


