extends Node2D

@onready var label = $Text
# Called when the node enters the scene tree for the first time.
func _ready():
	label.text = " " + str(G.score) + " !"


func _on_button_pressed():
	G.score = 0
	get_tree().change_scene_to_file("res://main.tscn")
	
