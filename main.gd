extends Node2D

var balloons : Array[Sprite2D] = []
const timeout : float = 10
var runtime : float = 0.0
var screensize : Vector2i = Vector2i()
var time = 60.0

@onready var timer = $Time
@onready var score = $Score

# Called when the node enters the scene tree for the first time.
func _ready():
	screensize = DisplayServer.window_get_size()
#	var pos = randf_range(0,screensize.size.x)
#	pos = min(pos,$Sprite2D.get_rect().size.x)
#	$Sprite2D.position.x = pos
	pass # Replace with fun	ction body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if time <= 0:
		get_tree().change_scene_to_file("res://end.tscn")
	time -= delta
	timer.text = " " + str(floor(time * 10) / 10)
	score.text = " " + str(G.score)

func _physics_process(delta):
	pass
